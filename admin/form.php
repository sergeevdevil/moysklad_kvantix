<style type="text/css">
	.moysklad_kvantix .title {
	    font-size: 20px;
	    font-weight: bold;
	}

	.moysklad_kvantix {
		max-width: 550px;
	    background: #fff;
	    padding: 25px;
	    box-shadow: 1px 4px 10px #c5c5c5;
	    border-radius: 10px;
	}

	.form-line {
	    margin: 10px 0 15px;
	    width: 100%;
	    max-width: 500px;
	}

	.form-line_setting {
	    display: flex;
	    align-items: center;
	}

	.label {
	    margin-right: 30px;
	    width: 200px;
	}

	.form-line input {
	    margin-right: 25px !important;
	}
	.form-line button {
		margin-top: 30px;
	    width: 100%;
	    max-width: 150px;
	    margin-right: 30px;
	    padding: 10px 0px;
	    background: #8BC34A;
	    color: #fff;
	    border: 0;
	    border-radius: 5px;
	    cursor: pointer;
	    box-shadow: 0 2px 2.5px #8BC34A;
	    transition: 0.15s;
	    outline: none;
	}
	.form-line button:hover {
		box-shadow: none;
	}
	.disclamer {
	    max-width: 500px;
	    margin: 30px 0;
	    padding: 15px;
	    background: #bae7f3;
	    border-radius: 5px;
	    border: 1px solid #8bb8ef;
	}
	.form-line button.load_now {
	    background: #8bb8ef;
	    box-shadow: 0 2px 2.5px #8bb8ef;
	}
</style>
<div class="moysklad_kvantix">
	<p class="title">Остатки</p>
	<div class="form-line">
		<div class="form-line_setting">
			<div class="label">Включить выгрузку</div>
			<div class="control"><input type="checkbox" name="start_load"></div>
		</div>
	</div>
	<div class="form-line">
		<div class="form-line_setting">
			<div class="label">Выгружать каждые</div>
			<div class="control"><input type="text" name="period_load" value="0"> <span>минут</span></div>
		</div>
	</div>
	<div class="form-line">
		<div class="form-line_setting">
			<div class="label">Выгружать модификации товаров</div>
			<div class="control"><input type="checkbox" name="include_modifications"></div>
		</div>
	</div>
	<div class="form-line">
		<div class="form-line_setting">
			<div class="label">Группа товаров</div>
			<div class="control">
				<select name="group_products">
					<option name="audi">Audi</option>
				</select>
			</div>
		</div>
	</div>

	<p class="title">Цены</p>
	<div class="form-line">
		<div class="form-line_setting">
			<div class="label">Группа товаров</div>
			<div class="control">
				<select name="group_products">
					<option name="audi">Audi</option>
				</select>
			</div>
		</div>
	</div>

	<p class="title">Склады</p>
	<div class="disclamer">
		Если склад не указан, то в магазин будут выгружены общие остатки без детализации по конкретным складам. Нельзя указать родительский и дочерний склады одновременно.
	</div>
	<div class="form-line">
		<div class="form-line_setting">
			<div class="label">Склад</div>
			<div class="control">
				<select name="group_products">
					<option name="audi">Audi</option>
				</select>
			</div>
		</div>
	</div>

	<div class="form-line">
		<button class="moysklad_action load_now">Выгрузить сейчас</button>
		<button class="moysklad_action save_now">Сохранить</button>
	</div>
</div>