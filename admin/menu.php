<?php
/**
* Module: Модуль интеграции с "Мой Склад"
* Author: Веб-студия "Квантикс"
* Site: https://kvantix.ru/
* File: menu.php
* Version: 1.0.0
**/

use Bitrix\Main\ModuleManager;
use Bitrix\Main\Localization\Loc; 
AddEventHandler("main", "OnBuildGlobalMenu", "DoBuildGlobalMenu");
Loc::loadMessages(__FILE__);


if ($APPLICATION->GetGroupRight('conversion') == 'D') {
	return false;
} else {
    function DoBuildGlobalMenu(&$aGlobalMenu, &$aModuleMenu) {     
        $arRes = array(
            array(
                "menu_id" => "kvantix",
                "text" => Loc::getMessage('MOYSKLAD_KVANTIX_MENU_TEXT'),
                "title" => Loc::getMessage('MOYSKLAD_KVANTIX_MENU_TITLE'),
                "sort" => 1000,
                "icon" => "kvantix_menu_icon",
                "items_id" => "global_menu_kvantix",
                "items" => array(
                    array(
                        "text" => Loc::getMessage('MOYSKLAD_KVANTIX_MENU_TITLE'),
                        "more_url" => array(),
                        'sort' => 10,
                        "icon" => "kvantix_menu_icon",
						'items_id' => 'moysklad-options',
						"items" => array(
							array(
								'text' => Loc::getMessage('MOYSKLAD_KVANTIX_MENU_SETTINGS'),
								'sort' => 10,
								"url" => "/bitrix/admin/moysklad_kvantix.php?lang=".LANGUAGE_ID,
								'icon' => '',
								'items_id' => 'moysklad_kvantix',
							),
						)
                    ),   
                )
            ),
        );
    
        return $arRes;
    
    } 
}

