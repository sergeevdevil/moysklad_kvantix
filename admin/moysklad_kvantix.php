<?php
/**
* Module: Модуль интеграции с "Мой Склад"
* Author: Веб-студия "Квантикс"
* Site: https://kvantix.ru/
* File: moysklad_kvantix.php
* Version: 1.0.0
**/

require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_before.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_after.php');

require_once(__DIR__ . '/form.php');