<?php
/**
* Module: Модуль интеграции с "Мой Склад"
* Author: Веб-студия "Квантикс"
* Site: https://kvantix.ru/
* File: options.php
* Version: 1.0.0
**/


$MESS['MOYSKLAD_KVANTIX_OPTIONS_TAB_NAME'] = 'Настройки интеграции модуля';
$MESS['MOYSKLAD_KVANTIX_OPTIONS_AUTH'] = 'Аутенфикация';
$MESS['MOYSKLAD_KVANTIX_OPTIONS_AUTH_LOGIN'] = 'Логин от сервиса "Мой склад"';
$MESS['MOYSKLAD_KVANTIX_OPTIONS_AUTH_PASSWORD'] = 'Пароль от сервиса "Мой склад"';
$MESS['MOYSKLAD_KVANTIX_OPTIONS_AUTH_APIKEY'] = 'API-ключ от сервиса "Мой склад"';
$MESS['MOYSKLAD_KVANTIX_OPTIONS_INPUT_APPLY'] = 'Сохранить';
$MESS['MOYSKLAD_KVANTIX_OPTIONS_INPUT_DEFAULT'] = 'По умолчанию';
