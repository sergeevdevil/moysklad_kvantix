<?php
/**
* Module: Модуль интеграции с "Мой Склад"
* Author: Веб-студия "Квантикс"
* Site: https://kvantix.ru/
* File: menu.php
* Version: 1.0.0
**/

$MESS['MOYSKLAD_KVANTIX_MENU_TEXT'] = "Мой склад";
$MESS['MOYSKLAD_KVANTIX_MENU_TITLE'] = "Установки синхронизации";
$MESS['MOYSKLAD_KVANTIX_MENU_SETTINGS'] = "Настроить";