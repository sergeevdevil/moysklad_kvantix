<?php
/**
* Module: Модуль интеграции с "Мой Склад"
* Author: Веб-студия "Квантикс"
* Site: https://kvantix.ru/
* File: index.php
* Version: 1.0.0
**/

$MESS['MOYSKLAD_KVANTIX_MODULE_NAME'] = 'Мой Склад';
$MESS['MOYSKLAD_KVANTIX_MODULE_DESCRIPTION'] = 'Модуль интеграции с "Мой Склад" от веб-студии Kvantix';