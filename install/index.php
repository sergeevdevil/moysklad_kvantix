<?
/**
* Module: Модуль интеграции с "Мой Склад"
* Author: Веб-студия "Квантикс"
* Site: https://kvantix.ru/
* File: index.php
* Version: 1.0.0
**/


use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Config\Option;
use Bitrix\Main\EventManager;
use Bitrix\Main\Application;
use Bitrix\Main\IO\Directory;

Loc::loadMessages(__FILE__);

class moysklad_kvantix extends CModule{

	public function __construct(){

		if(file_exists(__DIR__."/version.php")){

			$arModuleVersion = array();

			include_once(__DIR__."/version.php");

			$this->MODULE_ID 		   = str_replace(".", ".", get_class($this));
			$this->MODULE_VERSION 	   = $arModuleVersion["VERSION"];
			$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
			$this->MODULE_NAME 		   = Loc::getMessage("MOYSKLAD_KVANTIX_MODULE_NAME");
			$this->MODULE_DESCRIPTION  = Loc::getMessage("MOYSKLAD_KVANTIX_MODULE_DESCRIPTION"); 
		}

		return false;
	}

	public function DoInstall(){
		ModuleManager::registerModule($this->MODULE_ID);
		copy(__DIR__."/moysklad_kvantix.php", $_SERVER['DOCUMENT_ROOT'] . '/bitrix/admin/moysklad_kvantix.php');
		return false;
	}

	public function InstallFiles(){

		return false;
	}

	public function InstallDB(){

		return false;
	}

	public function InstallEvents(){

		return false;
	}

	public function DoUninstall(){
		ModuleManager::unRegisterModule($this->MODULE_ID);
		unlink($_SERVER['DOCUMENT_ROOT'] . '/bitrix/admin/moysklad_kvantix.php');
		return false;
	}

	public function UnInstallFiles(){

		return false;
	}

	public function UnInstallDB(){

		return false;
	}

	public function UnInstallEvents(){

		return false;
	}
}