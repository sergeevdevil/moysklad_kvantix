<?php
/**
* Module: Модуль интеграции с "Мой Склад"
* Author: Веб-студия "Квантикс"
* Site: https://kvantix.ru/
* File: version.php
* Version: 1.0.0
**/


$arModuleVersion = array(
  	"VERSION"      => "1.0.0",
 	"VERSION_DATE" => "2020-10-28 11:00:00"
);
